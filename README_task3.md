Consider a scenario where you have created a script which would install a package on your machine based on the name that you have provided and a second script that should display the version of that package on your machine, In jenkins we have a job1 that would execute the script 1 and we would like the second job 'job2' that would execute the second script and we want job2 to be executed automatically as soon as the first job is completed successfully also we would like to place in a check where nobody can trigger a build for Job1 if Job2 is in progress and vice-versa

![](screenshots/18.png)

![](screenshots/19.png)

![](screenshots/20.png)

![](screenshots/21.png)

![](screenshots/22.png)

![](screenshots/23.png)

![](screenshots/24.png)

![](screenshots/25.png)

![](screenshots/26.png)


