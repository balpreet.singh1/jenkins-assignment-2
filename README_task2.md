# You should configure a custom e-mail notification that would send the details about the execution like:
- Job name
- Build number
- who triggered the build
- Execution status

# Step 1 
## Install email extension plugin.


# Step 2
## Go to manage Jenkins --> Configure System and configure the extended email notification as below.


![](screenshots/12.png)

![](screenshots/13.png)

![](screenshots/14.png)

# Step 3
## In your Job configuration, add a post build step, EDITABLE EMAIL NOTIFICATION.
## Click on Advanced
## In triggers, select Always.

![](screenshots/15.png)

![](screenshots/16.png)

# Step 4
## Build the Job and Verify. You should get the email notification as below.

![](screenshots/17.png)


