# 1 Consider a scenario where you have created a script which will install a certain package on your machine and we would like to automate that process using Jenkins but considering an organization perspective we would have 3 functional users in Jenkins as
- DevOps
- Dev
- Qa
The user DevOps is able to manage the Jenkins instance globally
The user Dev is able to manage the jobs in Jenkins
The user QA is only able to execute the jobs nothing else

# 1 Consider a scenario where you have created a script which will install a certain package on your machine

![](screenshots/1.png)

# Configuration of Job which will install our desired package.
## Use of string parameter and git repo.

![](screenshots/2.png)

![](screenshots/3.png)

## Execution of shell script cloned using gitlab repo above.

![](screenshots/4.png)

## After we click on build, Jenkins will ask us for package name.

![](screenshots/5.png)

## Console Output.

![](screenshots/6.png)

# Created Users
- DevOps
- Dev
- Qa

![](screenshots/7.png)

# Select Role-based Strategy under Global security.

![](screenshots/8.png)

# Go to Dashboard --> Manage and assign Roles

![](screenshots/9.png)

# Create 2 new roles ExecuteJobs and ManageJobs, provide them the following access

![](screenshots/10.png)

# Assign the roles as below.

![](screenshots/11.png)

## 1. This will enable user devops to manage jenkins instance globally.
## 2. The user dev will be able to manage jobs in jenkins.
## 3. The user Qa will only be able to execute jobs and nothing else.



